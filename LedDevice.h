#ifndef LedDevice_h
#define LedDevice_h

#include "Arduino.h"
#include "GDevice.h"

class LedDevice: public GDevice {
  private:
    unsigned long previousMillis = 0;
    unsigned long interval = 500;
    void blinker();

  public:
  	~LedDevice();
    LedDevice(const char* name);
    LedDevice(const char* name, uint8_t pin);
    
    void blink();
    void blink(unsigned long interval);
};

#endif
