#ifndef PIRSensor_h
#define PIRSensor_h

#include "Arduino.h"
#include "GSensorForUint16T.h"

#define GSENSOR_NO_MODE 0
#define GSENSOR_MORE_THEN 1
#define GSENSOR_LESS_THEN 2
#define GSENSOR_BETWEEN 3
#define GSENSOR_NOT_BETWEEN 4

#define GSENSOR_ANALOG_MODE 0
#define GSENSOR_DIGITAL_MODE 1

class PIRSensor: public GSensorForUint16T {
  protected:
  	uint8_t pin;
  	uint8_t analogMode = GSENSOR_DIGITAL_MODE;
  	
  public:
  	~PIRSensor();
  	PIRSensor(const char* name);
    PIRSensor(const char* name, uint8_t pin);
    PIRSensor(const char* name, uint8_t pin, uint8_t analogMode);
    
    void begin();
    void begin(uint8_t pin);
    void begin(uint8_t pin, uint8_t analogMode);
    
    uint8_t getPinout();
    
    uint16_t read();
};

#endif
