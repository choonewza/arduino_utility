#include "GSensorModeForFloat.h"

GSensorModeForFloat::~GSensorModeForFloat(){
//	Serial.println(F("Debug: GSensorModeForFloat is destroyed."));
}

GSensorModeForFloat::GSensorModeForFloat(): GSensorMode(){
	
}

void GSensorModeForFloat::enableMoreThenMode(float activeSensorValue){
	this->activeSensorValue = activeSensorValue;
	this->activeMode = GSENSOR_MORE_THEN;
}

void GSensorModeForFloat::enableLessThenMode(float activeSensorValue){
	this->activeSensorValue = activeSensorValue;
	this->activeMode = GSENSOR_LESS_THEN;
}

void GSensorModeForFloat::enableBetweenMode(float minActiveSensorValue, float maxActiveSensorValue){
	this->minActiveSensorValue = minActiveSensorValue;
	this->maxActiveSensorValue = maxActiveSensorValue;
	this->activeMode = GSENSOR_BETWEEN;
}

void GSensorModeForFloat::enableNotBetweenMode(float minActiveSensorValue, float maxActiveSensorValue){
	this->minActiveSensorValue = minActiveSensorValue;
	this->maxActiveSensorValue = maxActiveSensorValue;
	this->activeMode = GSENSOR_NOT_BETWEEN;
}

bool GSensorModeForFloat::isActive(float sensorValue){
	bool actived;
	
	if(this->activeMode == GSENSOR_NOT_BETWEEN){
		actived = (sensorValue < this->minActiveSensorValue) && (sensorValue > this->maxActiveSensorValue);
	}else if (this->activeMode == GSENSOR_BETWEEN){
		actived = (sensorValue >= this->minActiveSensorValue) && (sensorValue <= this->maxActiveSensorValue);
	}else if (this->activeMode == GSENSOR_LESS_THEN){
		actived = (sensorValue<= this->activeSensorValue);
	}else{
		actived = (sensorValue >= this->activeSensorValue);
	}
	
	return actived;
}

void GSensorModeForFloat::showInfo(){
	
}
