#ifndef TimeTicker_h
#define TimeTicker_h

#include "Arduino.h"

class TimeTicker {
  private:
    bool active = true;
    unsigned long previousMillis  = 0;

  public:
  	~TimeTicker();
    TimeTicker();
    
    void attach(unsigned long interval, void (*callback)());
    void detach();
    void reAttach();
};

#endif
