#include "GSensorForUint16T.h"

GSensorForUint16T::~GSensorForUint16T() {
//	Serial.println(F("Debug: GSensorForUint16T is destroyed."));
	removeTasks();
 	delete this->tasks;
}

GSensorForUint16T::GSensorForUint16T(const char* name): GSensor(name) {

}

void GSensorForUint16T::begin() {
  Serial.println(F("DEBUG: GSensorForUint16T::begin() is not implementation!!!"));
}

uint16_t GSensorForUint16T::getSensorValue() {
  return this->sensorValue;
}

void GSensorForUint16T::setSensorValue(uint16_t sensorValue) {
  this->sensorValue = sensorValue;
}

uint8_t GSensorForUint16T::getPinout() {
  Serial.println(F("DEBUG: GSensorForUint16T::getPinout() is not implementation!!!"));
  return 0;
}

uint16_t GSensorForUint16T::read() {
  Serial.println(F("DEBUG: GSensorForUint16T::read() is not implementation!!!"));
  return this->sensorValue;
}

void GSensorForUint16T::addTimeTask(TimeTaskSensor* task){
	this->tasks->add(task);
}

void GSensorForUint16T::addTimeTask(GDevice* devices[], uint8_t sizeOfDevices,uint16_t activeDuration, uint8_t dayOfWeekRepeat, uint32_t startTime, uint32_t endTime, GSensorMode* sensorMode){
	TimeTaskSensor* tt = new TimeTaskSensor();
	
	tt->setTimeTask(dayOfWeekRepeat, startTime, endTime);
	tt->setGSensorMode(sensorMode);
	tt->setActiveDuration(activeDuration);
	
	for(int i=0 ; i<sizeOfDevices ; i++){
	    tt->addDevice(devices[i]);
	}
	  
	addTimeTask(tt);
}

void GSensorForUint16T::addMoreThenTimeTask(GDevice* devices[], uint8_t sizeOfDevices, uint8_t dayOfWeekRepeat, uint32_t startTime, uint32_t endTime, uint16_t activeSensorValue){
  	addMoreThenTimeTask(devices, sizeOfDevices, 0, dayOfWeekRepeat, startTime, endTime, activeSensorValue);
}
void GSensorForUint16T::addLessThenTimeTask(GDevice* devices[], uint8_t sizeOfDevices, uint8_t dayOfWeekRepeat, uint32_t startTime, uint32_t endTime, uint16_t activeSensorValue){
	addLessThenTimeTask(devices, sizeOfDevices, 0, dayOfWeekRepeat, startTime, endTime, activeSensorValue);
}
void GSensorForUint16T::addBetweenTimeTask(GDevice* devices[], uint8_t sizeOfDevices, uint8_t dayOfWeekRepeat, uint32_t startTime, uint32_t endTime, uint16_t minActiveSensorValue, uint16_t maxActiveSensorValue){
	addBetweenTimeTask(devices, sizeOfDevices, 0, dayOfWeekRepeat, startTime, endTime, minActiveSensorValue, maxActiveSensorValue);
}
void GSensorForUint16T::addNotBetweenTimeTask(GDevice* devices[], uint8_t sizeOfDevices, uint8_t dayOfWeekRepeat, uint32_t startTime, uint32_t endTime, uint16_t minActiveSensorValue, uint16_t maxActiveSensorValue){
	addNotBetweenTimeTask(devices, sizeOfDevices, 0, dayOfWeekRepeat, startTime, endTime, minActiveSensorValue, maxActiveSensorValue);
}

void GSensorForUint16T::addMoreThenTimeTask(GDevice* devices[], uint8_t sizeOfDevices,uint16_t activeDuration, uint8_t dayOfWeekRepeat, uint32_t startTime, uint32_t endTime, uint16_t activeSensorValue){
	GSensorModeForUint16T* sensorMode = new GSensorModeForUint16T();
  	sensorMode->enableMoreThenMode(activeSensorValue);

  	addTimeTask(devices, sizeOfDevices, activeDuration, dayOfWeekRepeat, startTime, endTime, sensorMode);
}
void GSensorForUint16T::addLessThenTimeTask(GDevice* devices[], uint8_t sizeOfDevices,uint16_t activeDuration, uint8_t dayOfWeekRepeat, uint32_t startTime, uint32_t endTime, uint16_t activeSensorValue){
	GSensorModeForUint16T* sensorMode = new GSensorModeForUint16T();
  	sensorMode->enableLessThenMode(activeSensorValue);
  	
  	addTimeTask(devices, sizeOfDevices, activeDuration, dayOfWeekRepeat, startTime, endTime, sensorMode);
}
void GSensorForUint16T::addBetweenTimeTask(GDevice* devices[], uint8_t sizeOfDevices,uint16_t activeDuration, uint8_t dayOfWeekRepeat, uint32_t startTime, uint32_t endTime, uint16_t minActiveSensorValue, uint16_t maxActiveSensorValue){
	GSensorModeForUint16T* sensorMode = new GSensorModeForUint16T();
  	sensorMode->enableBetweenMode(minActiveSensorValue, maxActiveSensorValue);
  	
  	addTimeTask(devices, sizeOfDevices, activeDuration, dayOfWeekRepeat, startTime, endTime, sensorMode);
}
void GSensorForUint16T::addNotBetweenTimeTask(GDevice* devices[], uint8_t sizeOfDevices,uint16_t activeDuration, uint8_t dayOfWeekRepeat, uint32_t startTime, uint32_t endTime, uint16_t minActiveSensorValue, uint16_t maxActiveSensorValue){
	GSensorModeForUint16T* sensorMode = new GSensorModeForUint16T();
  	sensorMode->enableBetweenMode(minActiveSensorValue, maxActiveSensorValue);
  	
  	addTimeTask(devices, sizeOfDevices, activeDuration, dayOfWeekRepeat, startTime, endTime, sensorMode);
}

void GSensorForUint16T::removeTask(TimeTaskSensor* task){
	int index = -1;
  	for (int i = 0; i < this->tasks->size(); i++) {
		if(this->tasks->get(i) == task){
    		index = i;
      		break;
    	}
  	}
  
	if(index >= 0){
		TimeTaskSensor* taskRemoved = this->tasks->remove(index);
			
		Serial.print("=> The ");
		Serial.print(this->name);
		Serial.print(" sensor Task Removed. -> ");
		taskRemoved->showInfo(this->name.c_str());
	
		delete taskRemoved; //Clear Memory
  	}
}

void  GSensorForUint16T::removeTasks() {
	LinkedList<TimeTaskSensor*>* removeTasks = new LinkedList<TimeTaskSensor*>();
	
	for (int i = 0; i < this->tasks->size(); i++) {
		removeTasks->add(this->tasks->get(i));
	}
	
	for (int i = 0; i < removeTasks->size(); i++) {
		removeTask(removeTasks->get(i));
	}
	
	removeTasks->clear();
	delete removeTasks; //Clear Memory
}

void GSensorForUint16T::showTasks(){
	if(this->tasks->size() == 0){
		Serial.print("The ");
		Serial.print(this->name);
		Serial.println(" sensor do not have any task.");
		return;
	}
	
	Serial.print("The ");
	Serial.print(this->name);
	Serial.print(" sensor have ");
	Serial.print(this->tasks->size());
	Serial.println(" tasks.");
	
	for (int i = 0; i < this->tasks->size(); i++) {
		Serial.print("Tase: ");
		Serial.println(i);
			
		this->tasks->get(i)->showInfo(this->name.c_str());
	}
}

void GSensorForUint16T::doTasks(uint8_t dayOfWeek, uint8_t hour, uint8_t minute, uint8_t second){	
	doTasks(dayOfWeek, hour, minute, second, false);
}

void GSensorForUint16T::doTasks(uint8_t dayOfWeek, uint8_t hour, uint8_t minute, uint8_t second, bool withCurrentValue){	
	if(this->tasks->size() <= 0){
		return;
	}
	
	if(this->disableScheduled){
		Serial.print("=> The ");
		Serial.print(this->name);
		Serial.println(" sensor is disable scheduled!");
		return;
	}

	LinkedList<TimeTaskSensor*>* shouldDeleteTasks = new LinkedList<TimeTaskSensor*>();
	
	for (int i = 0; i < this->tasks->size(); i++) {
		
		if(withCurrentValue){
			read();
		}
		
		bool result = this->tasks->get(i)->doTask((uint16_t) this->sensorValue, dayOfWeek, hour, minute, second);
		
		if(result){
			this->isOnceTurnOffDevices = true;
		}else{
			if(this->isOnceTurnOffDevices){
				this->tasks->get(i)->doTurnOffDevices();
				this->isOnceTurnOffDevices = false;
			}
		}
		
		if(this->tasks->get(i)->getShouldDelete()){
			shouldDeleteTasks->add(this->tasks->get(i));
		}
	}
	
	for (int i = 0; i < shouldDeleteTasks->size(); i++) {
		removeTask(shouldDeleteTasks->get(i));
	}
	
	shouldDeleteTasks->clear();
	delete shouldDeleteTasks; //Clear Memory
}
