#ifndef Utility_h
#define Utility_h

#include "Arduino.h"

class Utility {
  public:
    static char * strToChar(String str);
    static String strPad(String str, int len, char *pad);
    static String intToHexString(int num, int len);
    static int strToHex(const char* str);
};

#endif
