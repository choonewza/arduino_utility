#ifndef GDevice_h
#define GDevice_h

#include "Arduino.h"
#include "LinkedList.h" //https://github.com/ivanseidel/LinkedList
#include "TimeTaskDevice.h"

#define GDEVICE_ACTIVE_LOW 0
#define GDEVICE_ACTIVE_HIGH 1

class GDevice {
  protected:
  	String name;
    uint8_t pin;
    bool isActiveLow = false;
    bool disableScheduled = false;
    bool isControlFromSensor = false;
    LinkedList<TimeTaskDevice*>* tasks = new LinkedList<TimeTaskDevice*>();
    
  public:
  	virtual ~GDevice();
  	
    GDevice(const char* name);
    GDevice(const char* name, uint8_t pin);
    GDevice(const char* name, uint8_t pin, uint8_t mode);
    
    void begin();
    void begin(uint8_t pin);
    void begin(uint8_t pin, uint8_t mode);
    uint8_t getState();
    uint8_t getPinout();
    String getName();
    bool getDisableScheduled();
    void setDisableScheduled(bool disableScheduled);
    void setActiveMode(uint8_t mode);
    bool isTurnOn();
	void addTask(TimeTaskDevice* task);
	void addTask(uint8_t dayOfWeekRepeat, uint32_t startTime, uint32_t endTime);
    void removeTask(TimeTaskDevice* task);
    void removeTasks();
    void showTasks();
    virtual void on();
    virtual void off();
    void doTasks(uint8_t dayOfWeek, uint8_t hour, uint8_t minute, uint8_t second);
	uint8_t sizeOfTasks();
	bool getIsControlFromSensor();
    void setIsControlFromSensor(bool isControlFromSensor);
	bool equal(uint8_t pin);
};

#endif
