#include "TimeTaskDevice.h"

TimeTaskDevice::~TimeTaskDevice(){
//	Serial.println(F("Debug: TimeTaskDevice is destroyed."));
}

TimeTaskDevice::TimeTaskDevice(): TimeTask() {

}

TimeTaskDevice::TimeTaskDevice(uint16_t activeDuration, uint8_t dayOfWeekRepeat, uint32_t startTime, uint32_t endTime): TimeTask(){
	this->activeDuration = activeDuration;
	this->dayOfWeekRepeat = dayOfWeekRepeat;
	this->startTime = startTime;
	this->endTime = endTime;
}
