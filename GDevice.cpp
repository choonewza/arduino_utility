#include "GDevice.h"

GDevice::~GDevice() {
//	Serial.println(F("Debug: GDevice is destroyed."));
	removeTasks();
	delete this->tasks;
}

GDevice::GDevice(const char* name) {
	this->name = String(name);
}

GDevice::GDevice(const char* name, uint8_t pin) {
  this->name = String(name);
  this->pin = pin;
}

GDevice::GDevice(const char* name, uint8_t pin, uint8_t mode) {
  this->name = String(name);
  this->pin = pin;
  this->isActiveLow = (mode == GDEVICE_ACTIVE_LOW) ? true : false;
}

void GDevice::begin() {
  pinMode(this->pin, OUTPUT);
  delay(100);
  off();	//Turn-off GDevice
}

void GDevice::begin(uint8_t pin) {
  this->pin = pin;
  begin();
}

void GDevice::begin(uint8_t pin, uint8_t mode){
  setActiveMode(mode);
  begin(pin);
}

uint8_t GDevice::getState() {
  return digitalRead(this->pin);
}

uint8_t GDevice::getPinout() {
  return this->pin;
}

String GDevice::getName(){
  return this->name;
}

bool GDevice::getDisableScheduled(){
	return this->disableScheduled;
}

void GDevice::setDisableScheduled(bool disableScheduled){
	this->disableScheduled = disableScheduled;
}
    
void GDevice::setActiveMode(uint8_t mode){
	if(mode == GDEVICE_ACTIVE_LOW){
		this->isActiveLow = true;
	}else{
		this->isActiveLow = false;
	}
}

bool GDevice::isTurnOn(){
	if (this->isActiveLow) {
		return digitalRead(this->pin) == LOW ? true : false;
	} else {
		return digitalRead(this->pin) == HIGH ? true : false;
	}
}

void  GDevice::addTask(TimeTaskDevice* task) {
  bool isExist = false;
  for (int i = 0; i < this->tasks->size(); i++) {
  	bool isEqual = (this->tasks->get(i)->getDayOfWeekRepeat() == task->getDayOfWeekRepeat()) 
	  					&& (this->tasks->get(i)->getStartTime() == task->getStartTime()) 
						&& (this->tasks->get(i)->getEndTime() == task->getEndTime());
    if (isEqual) {
      isExist = true;
      break;
    }
  }
  
  if(isExist){
  	delete task;
  	return;	
  }
  
  this->tasks->add(task);
}

void GDevice::addTask(uint8_t dayOfWeekRepeat, uint32_t startTime, uint32_t endTime){
	TimeTaskDevice* tt = new TimeTaskDevice();
	tt->setTimeTask(dayOfWeekRepeat, startTime, endTime);
	addTask(tt);
}


void GDevice::removeTask(TimeTaskDevice* task){
	int index = -1;
  	for (int i = 0; i < this->tasks->size(); i++) {
		if(this->tasks->get(i) == task){
    		index = i;
      		break;
    	}
  	}
  
	if(index >= 0){
		TimeTaskDevice* taskRemoved = this->tasks->remove(index);
			
		Serial.print("=> The ");
		Serial.print(this->name);
		Serial.print(" sensor Task Removed. -> ");
		taskRemoved->showInfo(this->name.c_str());
	
		delete taskRemoved; //Clear Memory
  	}
}

void GDevice::removeTasks() {
	
	off(); //Turn-Off Device
	
	LinkedList<TimeTaskDevice*>* removeTasks = new LinkedList<TimeTaskDevice*>();
	
	for (int i = 0; i < this->tasks->size(); i++) {
		removeTasks->add(this->tasks->get(i));
	}
	
	for (int i = 0; i < removeTasks->size(); i++) {
		removeTask(removeTasks->get(i));
	}
	
	removeTasks->clear();
	delete removeTasks; //Clear Memory
}

void GDevice::showTasks(){
	if(this->tasks->size() == 0){
		Serial.print("The ");
		Serial.print(this->name);
		Serial.println(" do not have any task.");
		return;
	}
	for (int i = 0; i < this->tasks->size(); i++) {
		Serial.print("The ");
		Serial.print(this->name);
		Serial.print(" task is [");
		Serial.print(this->tasks->get(i)->getDayOfWeekRepeat());
		Serial.print(", ");
		Serial.print(this->tasks->get(i)->getStartTime());
		Serial.print(" to ");
		Serial.print(this->tasks->get(i)->getEndTime());
		Serial.println("]");
	}
}

void GDevice::on() {
  if (this->isActiveLow) {
    if(digitalRead(this->pin) == HIGH){
    	digitalWrite(this->pin, LOW);
    	
    	Serial.print(this->name);
		Serial.println(" is TURN-ON");
	}
  } else {
  	if(digitalRead(this->pin) == LOW){
    	digitalWrite(this->pin, HIGH);
    	
    	Serial.print(this->name);
		Serial.println(" is TURN-ON");
	}
  }
}

void GDevice::off() {
  if (this->isActiveLow) {
  	if(digitalRead(this->pin) == LOW){
    	digitalWrite(this->pin, HIGH);
    	
    	Serial.print(this->name);
		Serial.println(" is TURN-OFF");
	}
  } else {
  	if(digitalRead(this->pin) == HIGH){
    	digitalWrite(this->pin, LOW);
    	
    	Serial.print(this->name);
		Serial.println(" is TURN-OFF");
	}
  }
}

void GDevice::doTasks(uint8_t dayOfWeek, uint8_t hour, uint8_t minute, uint8_t second){
	
	if(this->isControlFromSensor){
		return;
	}
	
	if(this->disableScheduled){
		Serial.print("=> The ");
		Serial.print(this->name);
		Serial.println(" is disable scheduled!");
		return;
	}
	
	LinkedList<TimeTaskDevice*>* shouldDeleteTasks = new LinkedList<TimeTaskDevice*>();
	
	bool turnOn = false;
		
	for (int i = 0; i < this->tasks->size(); i++) {
		
		bool result = this->tasks->get(i)->doTask(dayOfWeek, hour, minute, second);
		
		if(result){
			turnOn = true;
			break;
		}
		
		if(this->tasks->get(i)->getShouldDelete()){
			shouldDeleteTasks->add(this->tasks->get(i));
		}
	}
	
	if(turnOn){
		on();
	}else{
		off();
	}
	
	for (int i = 0; i < shouldDeleteTasks->size(); i++) {
		removeTask(shouldDeleteTasks->get(i));
	}
	
	shouldDeleteTasks->clear();
	delete shouldDeleteTasks; //Clear Memory
}

uint8_t GDevice::sizeOfTasks(){
	return this->tasks->size();
}

bool GDevice::getIsControlFromSensor(){
	return this->isControlFromSensor;
}

void GDevice::setIsControlFromSensor(bool isControlFromSensor){
	this->isControlFromSensor = isControlFromSensor;
}

bool GDevice::equal(uint8_t pin){
	return this->pin == pin;
}
