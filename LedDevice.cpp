#include "LedDevice.h"

LedDevice::~LedDevice(){
//	Serial.println(F("Debug: LedDevice is destroyed."));
}

LedDevice::LedDevice(const char* name): GDevice(name) {

}

LedDevice::LedDevice(const char* name, uint8_t pin): GDevice(name, pin) {

}

void LedDevice::blinker() {
  if (digitalRead(this->pin) == LOW) {
    this->on();
  } else {
    this->off();
  }
}

void LedDevice::blink() {
  unsigned long currentMillis = millis();
  if (currentMillis - this->previousMillis >= this->interval) {
    // save the last time you blinked the LED
    this->previousMillis = currentMillis;
    this->blinker();
  }
}

void LedDevice::blink(unsigned long interval) {
  this->interval = interval;
  this->blink();
}
