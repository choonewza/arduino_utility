#ifndef TimeTaskSensor_h
#define TimeTaskSensor_h

#include "Arduino.h"

#include "GSensorMode.h"
#include "TimeTask.h"
#include "Utility.h"
#include "LinkedList.h" //https://github.com/ivanseidel/LinkedList
#include "GDevice.h"

#define GSENSOR_NO_MODE 0
#define GSENSOR_MORE_THEN 1
#define GSENSOR_LESS_THEN 2
#define GSENSOR_BETWEEN 3
#define GSENSOR_NOT_BETWEEN 4

class TimeTaskSensor:public TimeTask {
  protected:
    LinkedList<GDevice*>* devices = new LinkedList<GDevice*>();
    GSensorMode* sensorMode;

  public:
  	~TimeTaskSensor();
    TimeTaskSensor();
    
    TimeTaskSensor(GSensorMode* sensorMode, uint16_t activeDuration, uint8_t dayOfWeekRepeat, uint32_t startTime, uint32_t endTime);
    
    LinkedList<GDevice*>* getDevices();
    
    GSensorMode* getGSensorMode();
    void setGSensorMode(GSensorMode* sensorMode);
    
    void addDevice(GDevice* device);
    void removeDevice(GDevice* device);
    void removeDevice(uint8_t pin);
    void removeDevices();

    void doDevicesWhenActive(bool active);
    void doTurnOffDevices();
    bool doTask(bool active, uint8_t dayOfWeek, uint8_t hour, uint8_t minute, uint8_t second);
    
    bool doTask(uint16_t sensorValue, uint8_t dayOfWeek, uint8_t hour, uint8_t minute, uint8_t second);
    bool doTask(float sensorValue, uint8_t dayOfWeek, uint8_t hour, uint8_t minute, uint8_t second);
    
    void showInfo(const char* name);
    
    void destroy();
     
};

#endif
