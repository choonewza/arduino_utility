#include "GSensorModeForUint16T.h"

GSensorModeForUint16T::~GSensorModeForUint16T(){
//	Serial.println(F("Debug: GSensorModeForUint16T is destroyed."));
}

GSensorModeForUint16T::GSensorModeForUint16T(): GSensorMode(){
	
}

void GSensorModeForUint16T::enableMoreThenMode(uint16_t activeSensorValue){
	this->activeSensorValue = activeSensorValue;
	this->activeMode = GSENSOR_MORE_THEN;
}

void GSensorModeForUint16T::enableLessThenMode(uint16_t activeSensorValue){
	this->activeSensorValue = activeSensorValue;
	this->activeMode = GSENSOR_LESS_THEN;
}

void GSensorModeForUint16T::enableBetweenMode(uint16_t minActiveSensorValue, uint16_t maxActiveSensorValue){
	this->minActiveSensorValue = minActiveSensorValue;
	this->maxActiveSensorValue = maxActiveSensorValue;
	this->activeMode = GSENSOR_BETWEEN;
}

void GSensorModeForUint16T::enableNotBetweenMode(uint16_t minActiveSensorValue, uint16_t maxActiveSensorValue){
	this->minActiveSensorValue = minActiveSensorValue;
	this->maxActiveSensorValue = maxActiveSensorValue;
	this->activeMode = GSENSOR_NOT_BETWEEN;
}

bool GSensorModeForUint16T::isActive(uint16_t sensorValue){
	bool actived;
	
	if(this->activeMode == GSENSOR_NOT_BETWEEN){
		actived = (sensorValue < this->minActiveSensorValue) && (sensorValue > this->maxActiveSensorValue);
	}else if (this->activeMode == GSENSOR_BETWEEN){
		actived = (sensorValue >= this->minActiveSensorValue) && (sensorValue <= this->maxActiveSensorValue);
	}else if (this->activeMode == GSENSOR_LESS_THEN){
		actived = (sensorValue<= this->activeSensorValue);
	}else{
		actived = (sensorValue >= this->activeSensorValue);
	}
	
	return actived;
}

void GSensorModeForUint16T::showInfo(){
	switch(this->activeMode){
		case GSENSOR_MORE_THEN:
			Serial.print("  -> MORE_THEN Mode [activeSensorValue] = [");
			Serial.print(activeSensorValue);
			Serial.println("]");
			break;
		case GSENSOR_LESS_THEN:
			Serial.print("  -> LESS_THEN Mode [activeSensorValue] = [");
			Serial.print(activeSensorValue);
			Serial.println("]");
			break;
		case GSENSOR_BETWEEN:
			Serial.print("  -> BETWEEN Mode [minActiveSensorValue , maxActiveSensorValue] = [");
			Serial.print(minActiveSensorValue);
			Serial.print(", ");
			Serial.print(maxActiveSensorValue);
			Serial.println("]");
			break;
		case GSENSOR_NOT_BETWEEN:
			Serial.print("  -> NOT_BETWEEN Mode [minActiveSensorValue , maxActiveSensorValue] = [");
			Serial.print(minActiveSensorValue);
			Serial.print(", ");
			Serial.print(maxActiveSensorValue);
			Serial.println("]");
			break;
		
	}
}
