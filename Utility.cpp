#include "Utility.h"

char * Utility::strToChar(String str) {
  int len = str.length() + 1;
  char * buf = (char *) malloc (len);
  str.toCharArray(buf, len);
  return buf;
}

String Utility::strPad(String str, int len, char *pad) {
  String strTemp = "";
  String padd = String(pad);
  for (int i = str.length() ; i < len; i++) {
    strTemp += padd;
  }
  return strTemp + str;
}

String Utility::intToHexString(int num, int len){
	return Utility::strPad(String(num, HEX), len, "0");
}

int Utility::strToHex(const char* str){
  return (int) strtol(str, 0, 16);
}
