#ifndef GSensorModeForUint16T_h
#define GSensorModeForUint16T_h

#include "Arduino.h"
#include "GSensorMode.h"

#define GSENSOR_NO_MODE 0
#define GSENSOR_MORE_THEN 1
#define GSENSOR_LESS_THEN 2
#define GSENSOR_BETWEEN 3
#define GSENSOR_NOT_BETWEEN 4

class GSensorModeForUint16T: public GSensorMode  {
	protected:
		uint16_t activeSensorValue = 1;
		uint16_t minActiveSensorValue = 0;
		uint16_t maxActiveSensorValue = 0;
	
  public:
  	~GSensorModeForUint16T();
  	
	  GSensorModeForUint16T();
  
		void enableMoreThenMode(uint16_t activeSensorValue);
		void enableLessThenMode(uint16_t activeSensorValue);
		void enableBetweenMode(uint16_t minActiveSensorValue, uint16_t maxActiveSensorValue);
		void enableNotBetweenMode(uint16_t minActiveSensorValue, uint16_t maxActiveSensorValue);
		
		bool isActive(uint16_t sensorValue);
		
		void showInfo();
};

#endif
