#include "GSensor.h"

GSensor::~GSensor() {
//	Serial.println(F("Debug: GSensor is destroyed."));
	removeTasks();
 	delete this->tasks;
}

GSensor::GSensor(const char* name) {
	this->name = String(name);
}

bool GSensor::getDisableScheduled(){
	return this->disableScheduled;
}

void GSensor::setDisableScheduled(bool disableScheduled){
	this->disableScheduled = disableScheduled;
}

void GSensor::addTimeTask(TimeTaskSensor* task){
	this->tasks->add(task);
}

void GSensor::removeTask(TimeTaskSensor* task){
	int index = -1;
  	for (int i = 0; i < this->tasks->size(); i++) {
		if(this->tasks->get(i) == task){
    		index = i;
      		break;
    	}
  	}
  
	if(index >= 0){
		TimeTaskSensor* taskRemoved = this->tasks->remove(index);
			
		Serial.print("=> The ");
		Serial.print(this->name);
		Serial.print(" sensor Task Removed. -> ");
		taskRemoved->showInfo(this->name.c_str());
	
		delete taskRemoved; //Clear Memory
  	}
}

void  GSensor::removeTasks() {
	LinkedList<TimeTaskSensor*>* removeTasks = new LinkedList<TimeTaskSensor*>();
	
	for (int i = 0; i < this->tasks->size(); i++) {
		removeTasks->add(this->tasks->get(i));
	}
	
	for (int i = 0; i < removeTasks->size(); i++) {
		removeTask(removeTasks->get(i));
	}
	
	removeTasks->clear();
	delete removeTasks; //Clear Memory
}

void GSensor::showTasks(){
	if(this->tasks->size() == 0){
		Serial.print("The ");
		Serial.print(this->name);
		Serial.println(" sensor do not have any task.");
		return;
	}
	
	Serial.print("The ");
	Serial.print(this->name);
	Serial.print(" sensor have ");
	Serial.print(this->tasks->size());
	Serial.println(" tasks.");
	
	for (int i = 0; i < this->tasks->size(); i++) {
		Serial.print("Tase: ");
		Serial.println(i);
			
		this->tasks->get(i)->showInfo(this->name.c_str());
	}
}
