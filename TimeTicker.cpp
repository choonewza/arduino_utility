#include "TimeTicker.h"

TimeTicker::~TimeTicker() {
//	Serial.println(F("Debug: TimeTicker is destroyed."));
  	this->detach();
}

TimeTicker::TimeTicker() {

}

void TimeTicker::attach(unsigned long interval, void (*callback)()) {
  if (this->active == true) {
    unsigned long currentMillis = millis();
    if (currentMillis - this->previousMillis >= interval) {
      this->previousMillis = currentMillis;

      (*callback)(); // do callback function;
    }
  }
}

void TimeTicker::detach() {
  this->active = false;
  this->previousMillis = 0;
}

void TimeTicker::reAttach() {
  this->active = true;
}
