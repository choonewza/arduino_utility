#include "TimeTask.h"

TimeTask::~TimeTask(){
//	Serial.println(F("Debug: TimeTask is destroyed."));
}

TimeTask::TimeTask(){
	
}

uint8_t TimeTask::getDayOfWeekRepeat(){
	return this->dayOfWeekRepeat;
}
uint32_t TimeTask::getStartTime(){
	return this->startTime;	
}
uint32_t TimeTask::getEndTime(){
	return this->endTime;	
}
	
void TimeTask::setDayOfWeekRepeat(uint8_t dayOfWeekRepeat){
	this->dayOfWeekRepeat = dayOfWeekRepeat;
}

void TimeTask::setStartTime(uint8_t hour, uint8_t minute, uint8_t second){
    String sTime =  Utility::strPad(String(hour), 2,"0") + Utility::strPad(String(minute), 2,"0") + Utility::strPad(String(second), 2,"0");
    this->startTime = (uint32_t) sTime.toInt();
}

void TimeTask::setEndTime(uint8_t hour, uint8_t minute, uint8_t second){
	String eTime =  Utility::strPad(String(hour), 2,"0") + Utility::strPad(String(minute), 2,"0") + Utility::strPad(String(second), 2,"0");
    this->endTime = (uint32_t) eTime.toInt();
}

uint16_t TimeTask::getActiveDuration(){
	return this->activeDuration;
}
void TimeTask::setActiveDuration(uint16_t activeDuration){
	this->activeDuration = activeDuration;
}

void TimeTask::setTimeTask(uint8_t dayOfWeekRepeat, uint32_t startTime, uint32_t endTime){
	this->dayOfWeekRepeat = dayOfWeekRepeat;
	this->startTime = startTime;
	this->endTime = endTime;
}
	
bool TimeTask::doTask(uint8_t dayOfWeek, uint8_t hour, uint8_t minute, uint8_t second){
	
	
	String nTime =  Utility::strPad(String(hour), 2,"0") + Utility::strPad(String(minute), 2,"0") + Utility::strPad(String(second), 2,"0");
	uint32_t now =  nTime.toInt();
	
	//All time is active;
	if(this->dayOfWeekRepeat == 255){
		return true;
	}
    	
	if(this->dayOfWeekRepeat == 0 || checkDayOfWeek(dayOfWeek)){
		String nTime =  Utility::strPad(String(hour), 2,"0") + Utility::strPad(String(minute), 2,"0") + Utility::strPad(String(second), 2,"0");
		uint32_t now =  nTime.toInt();
		
		if(now >= this->startTime && now <= this->endTime){
			return true;
		}
		
	}
	
	if(this->dayOfWeekRepeat == 0 && this->endTime < now){
		this->shouldDelete = true;
	}
	
	return false;
}

bool TimeTask::getShouldDelete(){
	return this->shouldDelete;
}

bool TimeTask::checkDayOfWeek(uint8_t dayOfWeek){
	uint8_t dweek;
	
	switch(dayOfWeek){
		case 0: dweek = TIMETASK_SUNDAY;
			break;
		case 1: dweek = TIMETASK_MONDAY;
			break;
		case 2: dweek = TIMETASK_TUESDAY;
			break;
		case 3: dweek = TIMETASK_WEDNESDAY;
			break;
		case 4: dweek = TIMETASK_THURSDAY;
			break;
		case 5: dweek = TIMETASK_FRIDAY;
			break;
		case 6: dweek = TIMETASK_SATURDAY;
			break;
	}
	
	return ((this->dayOfWeekRepeat & dweek) != 0) ? true : false;
}

void TimeTask::showInfo(const char* name){
	Serial.print("Task of ");
	Serial.print(name);
	Serial.println(" ... ");
	
	Serial.print("  -> DateTime [dayOfWeekRepeat, startTime, endTime] = [");
	Serial.print(this->dayOfWeekRepeat);
	Serial.print(", ");
	Serial.print(this->startTime);
	Serial.print(", ");
	Serial.print(this->endTime);
	Serial.println("]");
	
	Serial.print("  -> Active Duration = ");
	Serial.println(this->activeDuration);
}
