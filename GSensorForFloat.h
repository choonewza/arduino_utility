#ifndef GSensorForFloat_h
#define GSensorForFloat_h

#include "Arduino.h"
#include "LinkedList.h" //https://github.com/ivanseidel/LinkedList
#include "GSensor.h"
#include "GDevice.h"
#include "TimeTaskSensor.h"
#include "GSensorModeForUint16T.h"

#define GSENSOR_NO_MODE 0
#define GSENSOR_MORE_THEN 1
#define GSENSOR_LESS_THEN 2
#define GSENSOR_BETWEEN 3
#define GSENSOR_NOT_BETWEEN 4

class GSensorForFloat: public GSensor {
  protected:
    float sensorValue = 0;
    
  public:
  	virtual ~GSensorForFloat();
  	
  	GSensorForFloat(const char* name);
    
    virtual void begin();
    
    float getSensorValue();
    
    void setSensorValue(float sensorValue);
    
    virtual uint8_t getPinout();
    
	virtual float read();
    
    void addTimeTask(TimeTaskSensor* task);
	void addTimeTask(GDevice* devices[], uint8_t sizeOfDevices, uint16_t activeDuration, uint8_t dayOfWeekRepeat, uint32_t startTime, uint32_t endTime, GSensorMode* sensorMode);
	
	void addMoreThenTimeTask(GDevice* devices[], uint8_t sizeOfDevices, uint8_t dayOfWeekRepeat, uint32_t startTime, uint32_t endTime, float activeSensorValue);
	void addLessThenTimeTask(GDevice* devices[], uint8_t sizeOfDevices, uint8_t dayOfWeekRepeat, uint32_t startTime, uint32_t endTime, float activeSensorValue);
	void addBetweenTimeTask(GDevice* devices[], uint8_t sizeOfDevices, uint8_t dayOfWeekRepeat, uint32_t startTime, uint32_t endTime, float minActiveSensorValue, float maxActiveSensorValue);
	void addNotBetweenTimeTask(GDevice* devices[], uint8_t sizeOfDevices, uint8_t dayOfWeekRepeat, uint32_t startTime, uint32_t endTime, float minActiveSensorValue, float maxActiveSensorValue);
	
	void addMoreThenTimeTask(GDevice* devices[], uint8_t sizeOfDevices, uint16_t activeDuration, uint8_t dayOfWeekRepeat, uint32_t startTime, uint32_t endTime, float activeSensorValue);
	void addLessThenTimeTask(GDevice* devices[], uint8_t sizeOfDevices, uint16_t activeDuration, uint8_t dayOfWeekRepeat, uint32_t startTime, uint32_t endTime, float activeSensorValue);
	void addBetweenTimeTask(GDevice* devices[], uint8_t sizeOfDevices, uint16_t activeDuration, uint8_t dayOfWeekRepeat, uint32_t startTime, uint32_t endTime, float minActiveSensorValue, float maxActiveSensorValue);
	void addNotBetweenTimeTask(GDevice* devices[], uint8_t sizeOfDevices, uint16_t activeDuration, uint8_t dayOfWeekRepeat, uint32_t startTime, uint32_t endTime, float minActiveSensorValue, float maxActiveSensorValue);
	
	void removeTask(TimeTaskSensor* task);
    void removeTasks();
    
    void showTasks();
    
    void doTasks(uint8_t dayOfWeek, uint8_t hour, uint8_t minute, uint8_t second);
    void doTasks(uint8_t dayOfWeek, uint8_t hour, uint8_t minute, uint8_t second, bool withCurrentValue);
};

#endif
