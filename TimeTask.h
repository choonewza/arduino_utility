#ifndef TimeTask_h
#define TimeTask_h

#include "Arduino.h"
#include "Utility.h"

#define TIMETASK_SUNDAY 1
#define TIMETASK_MONDAY 2
#define TIMETASK_TUESDAY 4
#define TIMETASK_WEDNESDAY 8
#define TIMETASK_THURSDAY 16
#define TIMETASK_FRIDAY 32
#define TIMETASK_SATURDAY 64

class TimeTask {
  protected:
    uint8_t dayOfWeekRepeat = 0; // Zero is no repeat.
    uint32_t startTime = 0;
    uint32_t endTime = 0;
    bool shouldDelete = false;
    bool checkDayOfWeek(uint8_t dayOfWeek);
    uint16_t activeDuration = 0; //milisec
    
	
    
  public:
  	virtual ~TimeTask();
    TimeTask();
    
	uint8_t getDayOfWeekRepeat();
	void setDayOfWeekRepeat(uint8_t dayOfWeekRepeat);
	
	uint32_t getStartTime();
	void setStartTime(uint8_t hour, uint8_t minute, uint8_t second);
	
	uint32_t getEndTime();
	void setEndTime(uint8_t hour, uint8_t minute, uint8_t second);
	
	uint16_t getActiveDuration();
	void setActiveDuration(uint16_t activeDuration);
	
	bool getShouldDelete();
	
	void setTimeTask(uint8_t dayOfWeekRepeat, uint32_t startTime, uint32_t endTime);
	
	virtual bool doTask(uint8_t dayOfWeek, uint8_t hour, uint8_t minute, uint8_t second);
	
	virtual void showInfo(const char* name);
};

#endif
