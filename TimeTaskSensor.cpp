#include "TimeTaskSensor.h"

TimeTaskSensor::~TimeTaskSensor() {
//	Serial.println(F("Debug: TimeTaskSensor is destroyed."));
	
	removeDevices();
	
	delete this->devices;
	delete this->sensorMode;
}

TimeTaskSensor::TimeTaskSensor(): TimeTask() {

}

TimeTaskSensor::TimeTaskSensor(GSensorMode* sensorMode, uint16_t activeDuration, uint8_t dayOfWeekRepeat, uint32_t startTime, uint32_t endTime): TimeTask(){
	this->sensorMode = sensorMode;
	this->activeDuration = activeDuration;
	this->dayOfWeekRepeat = dayOfWeekRepeat;
	this->startTime = startTime;
	this->endTime = endTime;
}

LinkedList<GDevice*>* TimeTaskSensor::getDevices(){
	return this->devices;
}

GSensorMode* TimeTaskSensor::getGSensorMode(){
	return this->sensorMode;
}

void TimeTaskSensor::setGSensorMode(GSensorMode* sensorMode){
	this->sensorMode = sensorMode;
}

void  TimeTaskSensor::addDevice(GDevice* device) {
	bool exits = false;
	for(int i=0; i<this->devices->size(); i++){
		if(this->devices->get(i)->getPinout() == device->getPinout()){
			exits = true;
			break;
		}
	}
	if(!exits){
		this->devices->add(device);
	}
}

void TimeTaskSensor::removeDevice(uint8_t pin){
  int index = -1;
  for (int i = 0; i < this->devices->size(); i++) {
	if (this->devices->get(i)->equal(pin)) {
      this->devices->get(i)->off();
      index = i;
      break;
    }
  }
  
  if(index >= 0){
  	GDevice *deviceRemoved = this->devices->remove(index);
  }
}

void  TimeTaskSensor::removeDevice(GDevice* device) {
  removeDevice(device->getPinout());
}

void TimeTaskSensor::removeDevices(){
	LinkedList<uint8_t> pinouts = LinkedList<uint8_t>();
	
	for (int i = 0; i < this->devices->size(); i++) {
		pinouts.add(this->devices->get(i)->getPinout());
	}
	
	for (int i = 0; i < pinouts.size(); i++) {
		removeDevice(pinouts.get(i));
	}
}

void TimeTaskSensor::doTurnOffDevices(){
    for (int i = 0; i < this->devices->size(); i++) {
      	this->devices->get(i)->setIsControlFromSensor(false); //set is sensor not perform
        this->devices->get(i)->off();
    }
}

void TimeTaskSensor::doDevicesWhenActive(bool active){
	if (active) {
      for (int i = 0; i < this->devices->size(); i++) {
      	this->devices->get(i)->setIsControlFromSensor(true); //set is sensor perform
        this->devices->get(i)->on();
        Serial.print("  - ");
        Serial.print(this->devices->get(i)->getName());
        Serial.println(" is TURN-ON.");
      }
    } else {
      for (int i = 0; i < this->devices->size(); i++) {
      	this->devices->get(i)->setIsControlFromSensor(false); //set is sensor not perform
        this->devices->get(i)->off();
      }
    }
}

bool TimeTaskSensor::doTask(bool active, uint8_t dayOfWeek, uint8_t hour, uint8_t minute, uint8_t second){
	bool isActive = TimeTask::doTask(dayOfWeek, hour, minute, second);
	
	if(isActive){
		doDevicesWhenActive(active);
		return active;
	}
	return false;
}

bool TimeTaskSensor::doTask(uint16_t sensorValue, uint8_t dayOfWeek, uint8_t hour, uint8_t minute, uint8_t second){
	bool isActive = sensorMode->isActive(sensorValue);
	
	return doTask(isActive, dayOfWeek, hour, minute, second);
}

bool TimeTaskSensor::doTask(float sensorValue, uint8_t dayOfWeek, uint8_t hour, uint8_t minute, uint8_t second){
	bool isActive = sensorMode->isActive(sensorValue);
	
	return doTask(isActive, dayOfWeek, hour, minute, second);
}

void TimeTaskSensor::showInfo(const char* name){
	TimeTask::showInfo(name);
	
	this->sensorMode->showInfo();
	
	if(this->devices->size() == 0){
		Serial.println("  -> Control devices [This task do not have any device.]");
		return;
	}
	
	Serial.print("  -> Control devices [");
	for (int i = 0; i < this->devices->size(); i++) {
		Serial.print(this->devices->get(i)->getName());
		
		if(i != this->devices->size()-1){
			Serial.print(", ");
		}
	}
	Serial.println("]");
    
}

void TimeTaskSensor::destroy(){
	Serial.println("<----------------------- TimeTaskSensor::destroy ----------------------->");
	removeDevices();
	
	delete this->devices;
	delete this->sensorMode;
}
