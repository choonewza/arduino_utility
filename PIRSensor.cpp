#include "PIRSensor.h"

PIRSensor::~PIRSensor(){
//	Serial.println(F("Debug: PIRSensor is destroyed."));
}

PIRSensor::PIRSensor(const char* name): GSensorForUint16T(name) {

}
PIRSensor::PIRSensor(const char* name, uint8_t pin): GSensorForUint16T(name) {
	this->pin = pin;
}
PIRSensor::PIRSensor(const char* name, uint8_t pin, uint8_t analogMode): GSensorForUint16T(name) {
  this->pin = pin;
  this->analogMode = analogMode;
}



void PIRSensor::begin() {
  if (this->analogMode == GSENSOR_DIGITAL_MODE) {
    pinMode(this->pin, INPUT);
  }
}

void PIRSensor::begin(uint8_t pin) {
  this->pin = pin;
  begin();
}

void PIRSensor::begin(uint8_t pin, uint8_t analogMode) {
  this->analogMode = analogMode;
  begin(pin);
}


uint8_t PIRSensor::getPinout() {
  return this->pin;
}

uint16_t PIRSensor::read() {
  this->sensorValue = (this->analogMode == GSENSOR_DIGITAL_MODE) ? (uint16_t) digitalRead(this->pin) : (uint16_t) analogRead(this->pin);
  return this->sensorValue;
}
