#ifndef RelayDevice_h
#define RelayDevice_h

#include "Arduino.h"
#include "GDevice.h"

class RelayDevice: public GDevice {
  protected:
    bool isActiveLow = true;

  public:
  	~RelayDevice();
    RelayDevice(const char* name);
    RelayDevice(const char* name, uint8_t pin);
    RelayDevice(const char*name, uint8_t pin, uint8_t mode);
};

#endif
