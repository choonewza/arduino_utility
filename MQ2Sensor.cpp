#include "MQ2Sensor.h"

MQ2Sensor::~MQ2Sensor(){
//	Serial.println(F("Debug: MQ2Sensor is destroyed."));
}

MQ2Sensor::MQ2Sensor(const char* name): GSensorForUint16T(name) {

}
MQ2Sensor::MQ2Sensor(const char* name, uint8_t pin): GSensorForUint16T(name) {
	this->pin = pin;
}
MQ2Sensor::MQ2Sensor(const char* name, uint8_t pin, uint8_t analogMode): GSensorForUint16T(name) {
  this->pin = pin;
  this->analogMode = analogMode;
}



void MQ2Sensor::begin() {
  if (this->analogMode == GSENSOR_DIGITAL_MODE) {
    pinMode(this->pin, INPUT);
  }
}

void MQ2Sensor::begin(uint8_t pin) {
  this->pin = pin;
  begin();
}

void MQ2Sensor::begin(uint8_t pin, uint8_t analogMode) {
  this->analogMode = analogMode;
  begin(pin);
}


uint8_t MQ2Sensor::getPinout() {
  return this->pin;
}

uint16_t MQ2Sensor::read() {
  this->sensorValue = (this->analogMode == GSENSOR_DIGITAL_MODE) ? (uint16_t) digitalRead(this->pin) : (uint16_t) analogRead(this->pin);
  return this->sensorValue;
}
