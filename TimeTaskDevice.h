#ifndef TimeTaskDevice_h
#define TimeTaskDevice_h

#include "Arduino.h"
#include "TimeTask.h"
#include "Utility.h"

class TimeTaskDevice:public TimeTask {
  protected:

  public:
  	~TimeTaskDevice();
    TimeTaskDevice();
    
    TimeTaskDevice(uint16_t activeDuration, uint8_t dayOfWeekRepeat, uint32_t startTime, uint32_t endTime);
};

#endif
