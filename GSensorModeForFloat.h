#ifndef GSensorModeForFloat_h
#define GSensorModeForFloat_h

#include "Arduino.h"
#include "GSensorMode.h"

#define GSENSOR_NO_MODE 0
#define GSENSOR_MORE_THEN 1
#define GSENSOR_LESS_THEN 2
#define GSENSOR_BETWEEN 3
#define GSENSOR_NOT_BETWEEN 4

class GSensorModeForFloat: public GSensorMode  {
  protected:
		float activeSensorValue = 0;
		float minActiveSensorValue = 0;
		float maxActiveSensorValue = 0;
	
  public:
  	~GSensorModeForFloat();
  	GSensorModeForFloat();
  	
	void enableMoreThenMode(float activeSensorValue);
	void enableLessThenMode(float activeSensorValue);
	void enableBetweenMode(float minActiveSensorValue, float maxActiveSensorValue);
	void enableNotBetweenMode(float minActiveSensorValue, float maxActiveSensorValue);
	
	bool isActive(float sensorValue);
	
	void showInfo();
};

#endif
