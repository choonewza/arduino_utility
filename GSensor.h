#ifndef GSensor_h
#define GSensor_h

#include "Arduino.h"
#include "LinkedList.h" //https://github.com/ivanseidel/LinkedList
#include "GDevice.h"
#include "TimeTaskSensor.h"

class GSensor {
  protected:
  	String name;
    
    bool isOnceTurnOffDevices = false;
    bool disableScheduled = false;

    LinkedList<TimeTaskSensor*>* tasks = new LinkedList<TimeTaskSensor*>();

  public:
  	virtual ~GSensor();
  	
  	GSensor(const char* name);
    
    bool getDisableScheduled();
    void setDisableScheduled(bool disableScheduled);
    
    void addTimeTask(TimeTaskSensor* task);

	void removeTask(TimeTaskSensor* task);
    void removeTasks();
    
    void showTasks();
    
    virtual void doTasks(uint8_t dayOfWeek, uint8_t hour, uint8_t minute, uint8_t second, bool withCurrentValue) = 0;
};

#endif
