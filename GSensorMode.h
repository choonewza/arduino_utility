#ifndef GSensorMode_h
#define GSensorMode_h

#include "Arduino.h"

#define GSENSOR_NO_MODE 0
#define GSENSOR_MORE_THEN 1
#define GSENSOR_LESS_THEN 2
#define GSENSOR_BETWEEN 3
#define GSENSOR_NOT_BETWEEN 4

class GSensorMode {
	protected:
		String name;
		uint8_t activeMode = GSENSOR_NO_MODE;
	
  public:
  	virtual ~GSensorMode();
  	GSensorMode();
  	
  	void setName(const char* name);
  	String getName();
	
	//For GSensorModeForUint16T
	virtual void enableMoreThenMode(uint16_t activeSensorValue);
	virtual void enableLessThenMode(uint16_t activeSensorValue);
	virtual void enableBetweenMode(uint16_t minActiveSensorValue, uint16_t maxActiveSensorValue);
	virtual void enableNotBetweenMode(uint16_t minActiveSensorValue, uint16_t maxActiveSensorValue);
	virtual bool isActive(uint16_t sensorValue);
	
	//For GSensorModeForFloat
	virtual void enableMoreThenMode(float activeSensorValue);
	virtual void enableLessThenMode(float activeSensorValue);
	virtual void enableBetweenMode(float minActiveSensorValue, float maxActiveSensorValue);
	virtual void enableNotBetweenMode(float minActiveSensorValue, float maxActiveSensorValue);
	virtual bool isActive(float sensorValue);
	
	
	//Pure virtual function
	virtual void showInfo() = 0;
};

#endif
